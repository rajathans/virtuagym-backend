 <?php

// Get
$router->get('plans', 'PlanController@index');
$router->get('users', 'UserController@index');
$router->get('exercises', 'ExerciseController@index');
$router->get('workouts', 'WorkoutController@index');

// Update
$router->put('plans/day', 'PlanController@addDay');

// Create
$router->post('users', 'UserController@store');
$router->post('plans', 'PlanController@store');
$router->post('exercises', 'ExerciseController@store');
$router->post('workouts', 'WorkoutController@store');


// Delete
$router->delete('users', 'UserController@delete');
$router->delete('plans', 'PlanController@delete');
$router->delete('exercises', 'ExerciseController@delete');
$router->delete('workouts', 'WorkoutController@delete');