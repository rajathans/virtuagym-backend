<?php

namespace App\Controllers;

use PDO;
use App\Core\Database\DB;

class PlanController
{
    public function index()
    {
        if (isset($_REQUEST['id'])) {
            echo response($this->show($_REQUEST['id']));
            return;
        }

        $results = (new DB)->execute("select * from plans");
        echo response([
            'data' => $results
        ]);
    }

    public function store()
    {
        $query = (new DB)->execute('insert into plans (plan_name, plan_description, plan_difficulty) values ("'. $_REQUEST['name'] .'","'. $_REQUEST['description'] .'","'. $_REQUEST['difficulty'] .'")', 'insert');

        echo response([
            'success' => true,
            'data' => [
                'id' => $query
            ]
        ]);
    }

    public function addDay()
    {
        $params     = file_get_contents('php://input');
        $data       = array();
        parse_str($params, $data);

        $exercises  = $data['exercises'];

        $max_sort       = (new DB)->execute('select max(sort) as max_sort from plan_days where plan_id=' . $data['plan_id']);
        $plan_day_id    = (new DB)->execute('insert into plan_days (plan_id, day_name, sort) values ("' . $data['plan_id'] . '", "' . $data['day_name'] . '","' . ((int) $max_sort[0]->max_sort + 1) . '");', 'insert');

        // Create exercise instances
        $original = 'insert into exercise_instances (exercise_id, day_id, exercise_duration, sort) values (';

        foreach ($exercises as $key => $exercise) {
            (new DB)->execute($original . '"' . $exercise . '", "'. $plan_day_id .'", "200", "' . ($key + 1) . '")', 'insert');
        }

        echo response([
            'success' => true
        ]);
    }

    public function delete()
    {
        if (!isset($_REQUEST['plan_id'])) {
			echo response([
				'success' => false
			]);

			return;
        }
        
        $data = (new DB)->execute("
			select user_plans.id as workout_id, users.*, plans.plan_name, plans.plan_description, plans.id as plan_id from user_plans 
			join plans on plans.id = user_plans.plan_id 
			join users on users.id = user_plans.user_id
			where plans.id=" . $_REQUEST['plan_id']
		);

		foreach ($data as $user_data) {
			var_dump($user_data);
			var_dump($user_data->first_name);
			(new \App\Core\Mailer)->send(
				$user_data->email,
				'Plan deletion noitification', 
				$user_data->first_name, 'Dear ' . $user_data->first_name .', plan ' . $data->plan_name . ' has been deleted.');
		}

        $db = new DB;
        $db->execute('
            delete from plans where id=' . $_REQUEST['plan_id'] .'; 
            delete from plan_days where plan_id=' . $_REQUEST['plan_id'] . ';
            delete from user_plans where plan_id=' . $_REQUEST['plan_id']. ';');

        echo response([
            'success' => true
        ]);
    }

    public function show()
    {
        $id = $_REQUEST['id'];

        $plan_data = (new DB)->execute('select * from plans where id=' . $id);
        $plan_days = (new DB)->execute('select * from plan_days where plan_id='.$id);   

        $days = [];
        foreach ($plan_days as $plan_day) {

            $exercises = (new DB)->execute('
                                            select exercises.id, exercises.name from exercise_instances 
                                            join exercises on exercises.id = exercise_instances.exercise_id 
                                            where exercise_instances.day_id=' . $plan_day->id
                                        );
            $ex = [];
            foreach ($exercises as $exercise) {

                if (!isset($ex[$exercise->id])) {
                    $ex[$exercise->id] = [
                        'id'    => $exercise->id,
                        'name'  => $exercise->name
                    ];
                } else {
                    $ex[$exercise->id][] = [
                        'id'    => $exercise->id,
                        'name'  => $exercise->name
                    ];
                }
            }

            $days[] = [
                'id' => $plan_day->id,
                'name'      => $plan_day->day_name,
                'day'       => $plan_day->sort,
                'exercises' => $exercises
            ];
        }
        
        echo response([
            'data' => [
                'id' => $plan_data[0]->id,
                'name' => $plan_data[0]->plan_name,
                'description' => $plan_data[0]->plan_description,
                'difficulty' => $plan_data[0]->plan_difficulty,
                'days' => $days
            ]
        ]);

        die;
    }
}
