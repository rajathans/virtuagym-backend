<?php

namespace App\Controllers;

use App\Core\Database\DB;
use App\Core\App;

class UserController
{
    /**
     * Show all users.
     */
    public function index()
    {
        $users = (new DB())->execute('select * from users');

        echo response([
            'data' => $users
        ]);
    }

    /**
     * Store a new user in the database.
     */
    public function store()
    {
        $string = 'insert into users (first_name, last_name, email) values ("' . $_REQUEST['first_name'] . '", "' .$_REQUEST['last_name'] . '", "' . $_REQUEST['email']. '")';

        (new DB())->execute($string);

        echo response([
            'success' => true
        ]);
    }

    public function delete()
    {
        if (!isset($_REQUEST['user_id'])) {
			echo response([
				'success' => false
			]);

			return;
		}

        $db = new DB;
        $db->execute('delete from users where id=' . $_REQUEST['user_id']);
        $db->execute('delete from user_plans where user_id=' . $_REQUEST['user_id']);

        echo response([
            'success' => true
        ]);
    }
}
