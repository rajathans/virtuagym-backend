<?php

namespace App\Controllers;

use PDO;
use App\Core\Database\DB;

class WorkoutController
{
    public function index()
    {
		$results = (new DB)->execute("
			select user_plans.id as workout_id, users.*, plans.plan_name, plans.plan_description, plans.id as plan_id from user_plans 
			join plans on plans.id = user_plans.plan_id 
			join users on users.id = user_plans.user_id"
		);

        echo response([
            'data' => $results
        ]);
	}

	public function delete()
    {
        if (!isset($_REQUEST['workout_id'])) {
			echo response([
				'success' => false
			]);

			return;
		}

		$data = (new DB)->execute("
			select user_plans.id as workout_id, users.*, plans.plan_name, plans.plan_description, plans.id as plan_id from user_plans 
			join plans on plans.id = user_plans.plan_id 
			join users on users.id = user_plans.user_id
			where user_plans.id=" . $_REQUEST['workout_id']
		);

		foreach ($data as $user_data) {
			var_dump($user_data);
			var_dump($user_data->first_name);
			(new \App\Core\Mailer)->send(
				$user_data->email,
				'Workout deletion noitification', 
				$user_data->first_name, 'Dear ' . $user_data->first_name .', workout ' . $data->plan_name . ' has been deleted.');
		}

        $db = new DB;
        $db->execute('delete from user_plans where id=' . $_REQUEST['workout_id']);

        echo response([
            'success' => true
        ]);
	}
	
	public function store()
    {
        $user_id = $_REQUEST['user_id'];
        $plan_id = $_REQUEST['plan_id'];

        (new DB)->execute('insert into user_plans (plan_id, user_id) values ("' . $plan_id . '","' . $user_id . '")', 'insert');

        echo response([
            'success' => true
        ]);
    }
}