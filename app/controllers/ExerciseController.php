<?php

namespace App\Controllers;

use PDO;
use App\Core\Database\DB;

class ExerciseController
{
    /**
     * Show the home page.
     */
    public function index()
    {
		$results = (new DB)->execute("select * from exercises;");
		
        echo response([
            'data' => $results
        ]);
    }

    /**
     * Show the about page.
     */
    public function store()
    {
        (new DB)->execute('insert into exercises (name) values ("' . $_REQUEST['exercise_name'] .'")', 'insert');

        echo response([
            'success' => true
        ]);
	}
	
	/**
     * Show the about page.
     */
    public function delete()
    {
		if (!isset($_REQUEST['exercise_id'])) {
			echo response([
				'success' => false
			]);

			return;
		}

        (new DB)->execute('delete from exercises where id=' . $_REQUEST['exercise_id']);

        echo response([
            'success' => true
        ]);
	}
}
