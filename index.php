<?php

header('Access-Control-Allow-Origin: *');
header("Content-type: application/json");
header("Access-Control-Allow-Methods", "GET", "PUT", "POST", "DELETE");

require 'vendor/autoload.php';
require 'core/bootstrap.php';

use App\Core\Router;
use App\Core\Request;

Router::load('app/routes.php')
    ->direct(Request::uri(), Request::method());