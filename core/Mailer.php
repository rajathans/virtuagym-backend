<?php

namespace App\Core;

use PHPMailer\PHPMailer;

class Mailer
{
    /**
     * @var \PHPMailer
     */
    protected $mail;

    public function __construct()
    {
        $config = App::get('config')['mail'];

        $this->mail = new PHPMailer\PHPMailer;

        $this->mail->isSMTP();
        $this->mail->Host       = $config['host'];
        $this->mail->SMTPAuth   = $config['smtpauth'];
        $this->mail->Port       = $config['port'];
        $this->mail->Username   = $config['username'];
        $this->mail->Password   = $config['password'];
    }

    public function send($email, $name, $subject, $message)
    {
        $this->mail->setFrom('info@virtuagym.com', 'Virtuagym');
        $this->mail->addAddress($email, $name);
        $this->mail->isHTML(true);

        $this->mail->Subject = $subject;
        $this->mail->Body   = $message;

        var_dump($this->mail->send());
        var_dump($this->mail->ErrorInfo);
    }

}