<?php

namespace App\Core\Database;

use PDO;
use PDOException;
use App\Core\App;

class DB
{
    public $connection;

    public function __construct()
    {
        $this->connection = $this->connect();
    }

    /**
     * Create a new PDO connection.
     *
     * @param array $config
     */
    public function connect()
    {
        $config = App::get('config')['database'];

        try {
            return new PDO(
                $config['connection'].';dbname='.$config['name'],
                $config['username'],
                $config['password'],
                $config['options']
            );
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function execute($string, $type = NULL)
    {
        $query = $this->connection->prepare($string);
        $query->execute();

        if ($type == 'insert') {
            return $this->connection->lastInsertId();
        }

        return $query->fetchAll(PDO::FETCH_CLASS);
    }
}
