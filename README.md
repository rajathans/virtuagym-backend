1. Clone backend and frontend api using git clone repo-url
2. Update config.php in backend folder with your keys
3. Import db.sql in your mysql server 
4. Start a web server for the backend from the backend root directory, on port 9000
5. Start a web server for the frontend from the frontend root directory on any port
